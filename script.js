// Беру все планеты
const planets = document.getElementsByClassName("planet");

for (let i = 0; i < planets.length; i++) {
  const planet = planets[i];

  // маусентер=ховер
  planet.addEventListener("mouseenter", function () {
    this.style.transform = "scale(1.2)";

    // из цсс показываю инфо
    const info = this.querySelector(".info");
    info.style.display = "block";
  });

  //  мауслив=ховер нету
  planet.addEventListener("mouseleave", function () {
    this.style.transform = "scale(1)";
    // прячу   инфо
    const info = this.querySelector(".info");
    info.style.display = "none";
  });
}
